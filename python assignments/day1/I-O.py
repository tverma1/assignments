#output
print(1, 2, 3, 4)
print(1, 2, 3, 4, sep='*') #by default space
print(1, 2, 3, 4, sep='$', end='@') #by default \n

#format mathod
x = 1; y = 2
print('The value of x is {} and y is {}'.format(x,y))#{} is a placeholder

#list
print('I want {0} and {1}'.format('bread','butter'))

#keyword for formating
print('Hello {name}'.format(name = 'taruna'))


# % operator
x = 694.3456789
print('The value of x is %.2f' %x)
print('The value of x is %.4f' %x)


#input
num = input('Enter a number: ')
print(type(num))
#int('num')
eval(num)
print(num)
print(type(num))

#import
import math
print(math.pi)

#from used
from math import pi
print(pi)


#pass
str=(1,2,3,4,5)
for x in str:
    pass