#global = 1
#@a=2
#1a=2
___=2
print(___)

#multiple statement
a = 1 + 2 + 3 + \
    4 + 5 + 6 + \
    7 + 8 + 9
print(a)


a = (1 + 2 + 3 +
    4 + 5 + 6 +
    7 + 8)
print(a)