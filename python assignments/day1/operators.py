x = 5
y = 3

#Arithmetic Operators
print('+',x + y)
print('-',x-y)
print('*',x*y)
print('%',x%y)
print('%',x/y)
print('**',x**y)
print('//',x//y)


# Assignment Operators

x &= y
print('&',x)

x=5
x |= y
print('|',x)

x=5
x ^= y
print('^',x)

x=5
x >>= y
print('>>',x)

x=5
x <<= y
print('>>',x)

#is operator
x = ["apple", "banana"]
y = ["apple", "banana"]
z = x

print(x is z)

# returns True because z is the same object as x

print(x is y)

# returns False because x is not the same object as y, even if they have the same content

print(x == y)

# this comparison returns True because x is equal to y


#in operator

print("banana" in x)
print("pineapple" not in x)
print("banana" not in x)
  

#Precedence and associativity
x=5;y=6;z=7
q =((x*y)+z-x)
print(q)