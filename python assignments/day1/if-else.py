
#if-else
a = 5
b = 10
if b > a:
  print("b is greater than a")


#elif
a = 5
b = 5
if b > a:
  print("b is greater than a")
elif a == b:
  print("a and b are equal")

#short hand if else
a = 2
b = 10
print("A") if a > b else print("B")

#and
a = 2
b = 3
c = 5
if a < b and c > a:
  print("(and)Both conditions are True")

#or
a = 2
b = 3
c = 5
if a > b or c > a:
  print("(or)Both conditions are True")

#pass
a = 3
b = 10

if b > a:
  pass


#for loop
p = ["apple", "banana", "cherry","apple"]
for x in p:
  print("for loop list:-",x)


p = {"apple", "banana", "cherry","apple"}
for x in p:
  print("for loop set:- ",x)


#while loop
i = 1
x = 5
print("while loop")
while i <= 10:
  print(x,"*",i,i*x)
  i += 1

#break
i = 1
x = 5
print("break")
while i <= 10:
  print(x,"*",i,i*x)
  if i==5:
    break
  i += 1

#continue
i = 0
x = 5
print("continue")
while i <= 5:
  i += 1
  if i==2:
     continue
  print(x,"*",i,i*x)
  
  