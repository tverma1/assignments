# str type
x = "Hello World"

#display x
print(x)

#display the data type of x
print(type(x)) 

# int type
x = 20

#display x:
print(x)

#display the data type of x:
print(type(x)) 

#float type
x = 20.5

#display x:
print(x)

#display the data type of x:
print(type(x)) 

#complex type
x = 3j

#display x:
print(x)

#display the data type of x:
print(type(x)) 

#list
x = ["apple", "banana", "cherry"]

#display x:
print(x)

#display the data type of x:
print(type(x)) 


#tuple
x = ("apple", "banana", "cherry")

#display x:
print(x)

#display the data type of x:
print(type(x)) 

#range
x = range(6)

#display x:
print(x)

#display the data type of x:
print(type(x)) 

#dict
x = {"name" : "John", "age" : 36}

#display x:
print(x)

#display the data type of x:
print(type(x)) 

#set
x = {"apple", "banana", "cherry","cherry"}

#display x:
print(x)

#display the data type of x:
print(type(x)) 


#boolean
x = True

#display x:
print(x)

#display the data type of x:
print(type(x)) 

#byte
x = b"Hello"

#display x:
print(x)

#display the data type of x:
print(type(x)) 

#byte array
x = bytearray(5)

#display x:
print(x)

#display the data type of x:
print(type(x)) 

#memoryview
x = memoryview(bytes(5))

#display x:
print(x)

#display the data type of x:
print(type(x)) 

#frozenset
x = frozenset({"apple", "banana", "cherry"})

#display x:
print(x)

#display the data type of x:
print(type(x)) 