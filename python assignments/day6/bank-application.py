#Banking Application



class User():
    
    def __init__(self, customer_name, moblie_no, age, gender):
        
        self.customer_name = customer_name
        self.moblie_no = moblie_no
        self.age = age
        self.gender = gender
        
    def show_details(self):
        print("Name: ", self.customer_name)
        print("Mobile Numbner", self.moblie_no)
        print("Age: ", self.age)
        print("Gender", self.gender)
        
        

class Bank(User):
    
    total_balance_of_bank = 100000
    
    @classmethod
    def update_total_balance_of_bank(cls, amount, transaction_str):
        if(transaction_str == 'deposite'):
            cls.total_balance_of_bank = cls.total_balance_of_bank + amount
        else:
            cls.total_balance_of_bank = cls.total_balance_of_bank - amount
            
    @classmethod
    def bank_status(cls):
        if cls.total_balance_of_bank == 0:
            print('\nBank is have no money')
        if cls.total_balance_of_bank < 0:
            print('\nBank balance in nagative')
        if cls.total_balance_of_bank > 0:
            print('\nBank balance is: ',cls.total_balance_of_bank)
            
        
    def __init__(self, account_no, bank_code, customer_name, moblie_no, age, gender, balance = 0):
        super().__init__(customer_name, moblie_no, age, gender)
        self.account_no = account_no
        self.bank_code = bank_code
        self.balance = balance
    
    def deposite(self, amount):
        self.amount = amount
        self.balance = self.balance + amount
        print(f"\nAmount deposite of {self.customer_name} : $", self.amount)
        Bank.update_total_balance_of_bank(amount,'deposite')
    
    def withdraw(self, amount):
        self.amount = amount
        if(self.amount > self.balance):
            print("\nInsufficient balance of your account $", self.balance)
        else:
            self.balance = self.balance - self.amount
            print(f"\nwithrows amount of {self.customer_name} $", self.amount)
            Bank.update_total_balance_of_bank(amount,'withdraw')
    
    def view_details(self):
        print("\nPersonal Details")
        print("====================")
        print("Account Number", self.account_no)
        print("Bank code", self.bank_code)
        self.show_details()
        print("Bank Balance", self.balance)
        print("====================")
        
    def view_balance(self):
        print(f"\nAvailable balance of {self.customer_name} is: ", self.balance)
        
bank_obj = Bank(120, 'PNB123', 'jimmi', '9090236182',  '33', 'F', 100)
bank_obj.view_details()
bank_obj.deposite(100)
bank_obj.withdraw(101)
bank_obj.view_balance()
Bank.bank_status()