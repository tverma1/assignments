from abc import ABC,abstractmethod

class Father(ABC):
    @abstractmethod
    def display(self):
        pass

    def show(self):
        print('concrete Meyhod')

class Child(Father):
    def display(self):
        print('child class')
        print('defining Abstract Method')


c = Child()
c.display()
c.show()