#class and object
class Person:
  def __init__(self, name, age):
     self.name = name
     self.age = age

  def myfunc(self):
    print("Hello my name is " + self.name)
    print("my age is ", self.age)

p1 = Person("jiny", 30)

#modify object
p1.age = 26
p1.myfunc()

#class and instance variable
class Dog:
    # Class Variable
    animal = 'dog'            
   
   # The init method or constructor
    def __init__(self, breed, color):
      # Instance Variable    
        self.breed = breed
        self.color = color       
    
# Objects of Dog class
Rodger = Dog("Pug", "brown")
Buzo = Dog("Bulldog", "black")
 
print('\nRodger details:')  
print('Rodger is a', Rodger.animal)
print('Breed: ', Rodger.breed)
print('Color: ', Rodger.color)
 
print('\nBuzo details:')  
print('Buzo is a', Buzo.animal)
print('Breed: ', Buzo.breed)
print('Color: ', Buzo.color)
 
# Class variables can be accessed using class name also
print("\nAccessing class variable using class name")
print(Dog.animal)       

# Now if we change the animal for just Rodger it won't be changed for Buzo
Rodger.animal = 'cat'
print('\nchange for Rodger')
print(Rodger.animal) # prints 'cat'
print(Buzo.animal) # prints 'dog'
  
# To change the animal for all instances of the class we can change it 
# directly from the class
Dog.animal = 'cat'
print('\nchange for all')
print(Rodger.animal) # prints 'cat'
print(Buzo.animal) # prints 'cat'

#class method without agrs

class Mobile:
  fp = 'yes'           #class variable
  
  @classmethod
  def show_model(cls): #class method
    print('calling class method without Args')
    print(cls.fp)      #accessing class variable using class method
    print('vivo V11')

vivo = Mobile()
Mobile.show_model()    #calling class method without 

#class method with agrs

class Mobile:
  fp = 'yes'           #class variable
  
  @classmethod
  def show_model(cls, r): #class method
    cls.ram = r
    print('calling class method withArgs')
    print(cls.fp, cls.ram)

vivo = Mobile()
Mobile.show_model('4GB')    #calling class method withArgs

#staticmethod 

import math

class Pizza:
    def __init__(self, radius, ingredients):
        self.radius = radius
        self.ingredients = ingredients

    def __repr__(self):
        return (f'Pizza({self.radius!r}, '
                f'{self.ingredients!r})')

    def area(self):
        return self.circle_area(self.radius)

    @staticmethod
    def circle_area(r):
        return r ** 2 * math.pi
      
p = Pizza(4, ['mozzarella', 'tomatoes'])

print(p.area())
print(Pizza.circle_area(4))
repr()
