import csv


class Reader:

    def read_csv_file(self):
        sales_tax_data = []
        try:
            with open('input.csv', 'r') as input:
                csv_reader = csv.reader(input)
                next(csv_reader)
                try:
                    for line in csv_reader:
                            sales_tax_data.append([line[0], line[1], line[2], line[3]]) 
                except IndexError:
                   print('input file not have product data row')
                   
        except FileNotFoundError:
            print('file not exit')
            
        return sales_tax_data  
    

class Validator(Reader):
    error = 'no'
    
    def validator_fun(self):
        line_count = 2
        sales_tax_validate_data = []
        sales_tax_data = self.read_csv_file()
        for x in sales_tax_data:
            product_name = x[0]
            product_cost = x[1]
            product_sales_tax_percentage = x[2]
            product_country = x[3]
            self.product_name_country_validator(product_name, 'product name')
            self.product_cost_sales_tax_percentage_validator(product_cost, 'product cost')
            self.product_cost_sales_tax_percentage_validator(product_sales_tax_percentage, 'product sales tax percentage')
            self.product_name_country_validator(product_country, 'country')
            if Validator.error == 'yes':
                print(f'please ckeck input file line number = {line_count}')
                break
            sales_tax_validate_data.append([product_name, product_cost, product_sales_tax_percentage, product_country]) 
            line_count += 1
        return sales_tax_validate_data
    
    def product_name_country_validator(self, product_name_country, product_name_country_str):
        try:
            if not product_name_country:
                raise RuntimeError
            
        except RuntimeError:
            print(f'{product_name_country_str} is not null and should be string')
            Validator.error = 'yes'
    
    def product_cost_sales_tax_percentage_validator(self, product_cost_sales_tax, product_cost_sales_tax_str):
         
        try:
            product_cost_sales_tax = float(product_cost_sales_tax)
        except ValueError:
            print(f'{product_cost_sales_tax_str} is must be number or not null')
            Validator.error = 'yes'
            return
        
        try:
            if product_cost_sales_tax <= 0:
                raise ValueError
        except ValueError as ve:
            print(f'{product_cost_sales_tax_str} must be positive number and greater than 0')
            Validator.error = 'yes'
 
        
class Calulate_sales_tax(Validator):
    
    def cal_tax(self):
        sales_tax_validate_data = self.validator_fun()
        sales_tax_final_cal_data = []
        for x in sales_tax_validate_data:
            sales_tax_amount = round(float(x[1]) * float(x[2]) / 100, 2)
            final_price = round(float(x[1]) + sales_tax_amount, 2)
            sales_tax_final_cal_data.append([x[0], x[1], sales_tax_amount, final_price, x[3]])  
        return sales_tax_final_cal_data
    
    
class Writer:
    
    def write_csv_file(self, sales_tax_final_cal_data):   
        fields = ['Product-Name', 'Product-CostPrice', 'Product-SalesTax', 'Product-FinalPrice', 'Country']
        with open('output.csv', 'w', newline = '') as output:
            csv_writer = csv.writer(output)
            #writing the fields
            csv_writer.writerow(fields)
            #writing the datarows
            csv_writer.writerows(sales_tax_final_cal_data)


class Driver(Calulate_sales_tax, Writer):

    def driver_fun(self):
        sales_tax_final_cal_data = self.cal_tax()
        if Validator.error == 'no':
            self.write_csv_file(sales_tax_final_cal_data)

d_obj = Driver()
d_obj.driver_fun()
        
