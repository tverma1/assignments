import csv

'''
calulate the sales tax and final price of diffrent product 
of diffrent country after appling tha sales tax
'''
class Sales_tax:
    error = 'no'
    def read_csv_file(self):
        sales_tax_validate_data = []
        try:
            with open('input.csv', 'r') as input:
                csv_reader = csv.reader(input)
                next(csv_reader)
                try:
                    for line in csv_reader:
                            product_name = line[0]
                            product_country = line[3]
                            self.product_name_country_validation(product_name, product_country)
                            if Sales_tax.error == 'yes':
                                break
                            product_cost = line[1]
                            product_sales_tax_per = line[2]
                            self.product_salestax_cost_validation(product_cost, product_sales_tax_per)
                            if Sales_tax.error == 'yes':
                                break
                            sales_tax_validate_data.append([product_name,product_cost,product_sales_tax_per,product_country]) 
                except IndexError:
                   print('input file not have product data row')
                   Sales_tax.error == 'yes'
                   
        except FileNotFoundError:
            print('file not exit')
            Sales_tax.error == 'yes'
            
        return sales_tax_validate_data  
     
    #validation of product name and country           
    def product_name_country_validation(self, product_name, product_country):
        try:
            if not product_name:
                raise RuntimeError('product name is not null and should be string')
            
            if not product_country:
                raise RuntimeError('product country must be string or not null')
         
        except RuntimeError as r:
            print(r.args)
            Sales_tax.error = 'yes'
        
    #validation of product cost and tax
    def product_salestax_cost_validation(self, product_cost, product_sales_tax_per):
        try:
            product_cost = float(product_cost)
        except ValueError:
            print('product cost must be number or not null')
            Sales_tax.error = 'yes'
            return
        try:
            product_sales_tax_per = float(product_sales_tax_per)
        except ValueError:
            print('product sales tax percentage must be number or not null')
            Sales_tax.error = 'yes'
            return
        try:
            if product_cost <= 0:
                raise ValueError('product cost must be positive number and greater than 0')
            if product_sales_tax_per < 0:
                raise ValueError('product sales tax percentage must be positive number or zero ')
        except ValueError as ve:
            print(ve.args)
            Sales_tax.error = 'yes'
            return
             
    #calculate tax and final price of product according to diffrent country(diffrent sales tax on diffrent product)                
    def cal_tax(self, sales_tax_validate_data):
        sales_tax_final_cal_data = []
        for x in sales_tax_validate_data:
            sales_tax_amount = round(float(x[1]) * float(x[2]) / 100,2)
            final_price = round(float(x[1]) + sales_tax_amount,2)
            sales_tax_final_cal_data.append([x[0],x[1],sales_tax_amount, final_price,x[3]])  
        return sales_tax_final_cal_data
      
    #witering output.csv file
    #final data after calulating tax and final price of product                     
    def write_csv_file(self, sales_tax_final_cal_data):   
        fields = ['Product-Name', 'Product-CostPrice', 'Product-SalesTax', 'Product-FinalPrice', 'Country']
        with open('output.csv', 'w', newline = '') as output:
            csv_writer = csv.writer(output)
            #writing the fields
            csv_writer.writerow(fields)
            #writing the datarows
            csv_writer.writerows(sales_tax_final_cal_data)
            
    def driver_fun(self):   
        sales_tax_validate_data = self.read_csv_file()
        sales_tax_final_cal_data = self.cal_tax(sales_tax_validate_data)
        if Sales_tax.error == 'no':
            self.write_csv_file(sales_tax_final_cal_data)
    
sales_tax_obj = Sales_tax()  
sales_tax_obj.driver_fun()