import csv

error = 'No'
tax = 10
sales_tax_cal_final_data = []

try:
    with open('input.csv', 'r') as input:
        csv_reader = csv.reader(input)
        next(csv_reader)
        try:
            for line in csv_reader:
                try:
                    #validation
                    product_name = line[0]
                    if not product_name:
                        raise RuntimeError('product name is not null and should be string')
                    
                    product_country = line[2]
                    if not product_country:
                        raise RuntimeError('product country must be string or not null')
                                
                except RuntimeError as re:
                    print(re.args)
                    error = 'yes'
                    break
                        
                try:
                    product_cost = float(line[1])
                except ValueError:
                    print('product cost must be number or not null')
                    error = 'yes'
                    break
                try:
                   if product_cost <= 0:
                        raise ValueError
                except ValueError:
                    print('product cost must be positive number and greater than 0')
                    error = 'yes'
                    break
                
                cost_price = line[1]
                sales_tax_amount = round(float(cost_price) * tax / 100,2)
                final_price = round(float(cost_price) + sales_tax_amount,2)

                #Product-Name, Product-CostPrice, Product-SalesTax, Product-FinalPrice, Country
                sales_tax_cal_final_data.append([line[0],line[1],sales_tax_amount, final_price,line[2]])

        except IndexError:
            print('input file not have product data row')
            error = 'yes'

except FileNotFoundError:
    print('file not exit')   
    error = 'yes'        

#writing sales data in csv file
if error == 'No':               
   fields = ['Product-Name', 'Product-CostPrice', 'Product-SalesTax', 'Product-FinalPrice', 'Country']

   with open('output.csv', 'w', newline = '') as output:
    csv_writer = csv.writer(output)

    #writing the fields
    csv_writer.writerow(fields)
        
    #writing the datarows
    csv_writer.writerows(sales_tax_cal_final_data)
        
        
        