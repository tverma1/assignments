
# my_range funtion to get the numbers of given range
'''
i = 0
def my_range(range):
    while(i < range):
        print(i)
        i = i + 1
'''
# print range without range()
def my_range(range, reverse = True):
    if reverse == True:
        i = range
        while(i >= 1):
           print(i)
           i = i - 1
    else:
        i = 0
        while(i < range):
           print(i)
           i = i + 1

my_range(10)
print("reverse = True")
my_range(10, True)
print("reverse = False")
my_range(10, False)

'''
# print range without range()
def my_range(range, reverse = True):
    if reverse == True:
        i = range
        while(i >= 1):
           yield i
           i = i - 1
    else:
        i = 0
        while(i < range):
           yield i
           i = i + 1

for i in my_range(20):
    print(i)

print("reverse = False")
for i in my_range(20, False):
    print(i)

'''