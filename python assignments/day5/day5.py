'''
iterable_value = 'hello i am taruna'
iterable_obj = iter(iterable_value)

while True:
    try:
    
         # Iterate by calling next
        item = next(iterable_obj)
        print(item)

    except StopIteration:
 
        # exception will happen when iteration will over
        print('end of iterable_value')
        break
'''
"""
#generator

def gen(n):
    for i in range(n):
        yield i 

'''
for i in gen(10000):
    print(i)
'''

obj1 = gen(4)
print(next(obj1))
print(next(obj1))
print(next(obj1))
print(next(obj1))
print(next(obj1))
print(next(obj1))

"""
#decoraors
def decore(fun):
    def inner():
        print('before enhancing function')
        fun() #num function called
        print('after enhancing function')
    return inner

def num():
    print("num function")

result = decore(num) #inner function ref.come here
result() #inner function called

#closure
def outer():
    x = 3 
    def inner():
        y = 4
        result = x + y
        return result
    return inner

a = outer()
print(a())