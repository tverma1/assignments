#single line string
str1="hello i am taruna"
print(str1)

#multiple line string
str2='''hello,
i am 
taruna'''
print(str2)

str3 ="hello i am taruna student of 'MCA'"
print(str3)

str3 ="hello, \ni am taruna student of 'MCA'"
print(str3)

#escape not work
str3 = r"hello, \n i am taruna student of 'MCA'"
print(str3)

#memory allocation
str4="taruna"
str5="taruna"
#same memory allocated (only assign not create new memory for exist one)
print("taruna",id(str4))
print("taruna",id(str5))
str6="Taruna"
print("Taruna",id(str6))


#accessing string
print(str4[0])
print(str4[1])
print(str4[2])
print(str4[3])
print(str4[4])
print(str4[5])

#by loop
for x in str4:
    print("for loop",x)

#access using range and len()
for i in range(len(str4)):
    print("using range and len()",str4[i])

#with loop
print("direct access:-",str4)

#using while loop
j=0
while j<len(str4):
    print("while loop",str4[j])
    j+=1

'''
#sting immutable
str4[1] = 'u'
print("modify string",str4)
'''

#repeting operation 
print("repeting operation:- ",str4 * 5)
print("by range",str4[0:4]*4)

#concatenation operation
s=str5+str6
print("concatenation",s)
print("hello,"+str4)#no space in output
print("hello,",str4)#space in output
print("Hello"+str4+"goodmorning")


#camparing String
str4="taruna"
str5="taruna"
str6="TARUNA"
result = str5==str4
print(result)

result = str5 > str6
print(result)

result = str5 < str6
print(result)

