set = {1,2,3,4,5,6}
print(set)
print(type(set))

#access items 
for i in set:
    print("for loop",i)


 #other set
set.add(10)
print("added set",set)

#update set (added two set )
set1 = {7,8,9,0}
set.update(set1)
print("added two set",set)
print("length of set",len(set))

#remove item
set.remove(1)
print("remove 1 from item",set)

set.discard(0)
print("discard 0 from item",set)

#pop method
x = set.pop()
print(x)
print("set after poped item",set)

#clear
set.clear()
print("set after clear",set)

#del
del set
print("set after delete",set)
print(set)