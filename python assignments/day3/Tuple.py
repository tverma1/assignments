# this is not a tuple its only a integer
a = (10)
print("this is not tuple :-")
print(a)
print(type(a))

# this is a tuple
a = (10,)
print("this is tuple :-")
print(a)
print(type(a))

#multi data type tuple
b = 1,9.5,'taruna',True #()not required and use because its show tuple
print('multi data type tuple')
print(b)
print(type(b))

#accessing of tuple by index
print('tuple a')
print(a[0])
#print(a[1]) out of range erroe

print('tuple b')
print(b[0])
print(b[-1])

#accesssing by for loop
print('using for loop')
for elements in b:
    print(elements)

#accesssing by for loop with index
n = len(b)
print('accesssing by for loop with index')
for i in range(n):
    print(i,b[i])

i=1
print('accesssing by for loop with  neg. index')
for i in range(n):
    print(i,b[-i])

# using while loop(for while loop you must know the length)
n = len(b)
i=0 #for increment
print('accesssing by while loop')
while i<n:
    print(b[i])
    i+=1

#tuple length
print('tuple length')
print(len(b))

#tuple constractor
b = ((2,4,5,6.3,'tanu'))# note the double round-brackets
print('Constructor of tuple')
print(b)