student = {
    "name":"jassy",
    "age": 20,
    "DOB" :"20/7/2000",

}
print(student)

#access
x = student["name"]
print("name:-",x)

#by get method 
x = student.get("age")
print("age by get method:-",x)

#key method
x = student.keys()
print("key method:-",x)

#added item in dictionary
student["lastname"] = "verma"
print("after added new item:-",student)
print("keys after added new item :-",x)

#get all values from dictionary
z = student.values()
print("values:-",z)

#get all item from dictionary
z = student.items()
print("items:-",z)

#check if key exists
if "name" in student:
    print("yes name key is exist")

#change dictionary
student["name"] = "tanu" 
print("after changing name:-",student)

#upadte method
student.update({"DOB":"26/12/1997"})
print("after update DOB:-",student)

#added item
student["course"] = 'MCA'
print(student)

#remove 
#pop()
student.pop("course")
print("after removing course",student)

#popitem()
student.popitem()
print("aftre popitem:-",student)

#del 
del student["age"]
print("after delete age",student)

'''
#clear()
student.clear()
print("after claer:- ",student)
'''

'''
#del whole  dictionary
del student
print(student)
'''

#for loop
for x in student:
    print("for loop iterate key",x)

for x in student:
    print("for loop iterate values",student[x])

for x in student.values():
    print("access using values()",x)

for x in student.keys():
    print("access using keys()",x)

for x,y in student.items():
    print("access using items()",x,y)