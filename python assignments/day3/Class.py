'''
class Student:
  def __init__(self, name, age):
    self.name = name
    self.age = age
  def myfun(self):
    print("hello "+self.name)

s1 = Student("Jassy", 36)

print(s1.name)
print(s1.age)
s1.myfun()

#modify age
s1.age = 40
print(s1.age)
'''

'''
#del properties
del s1.age
print(s1.age)
'''

'''
#del object
del s1
print(s1)
'''
"""
#single inheritance and constractor
class Parent:
    money = 1000
    def show(self):
         print("Parent Class Instance Method")
    @classmethod
    def showMoney(cls):
        print("Parent Class class Method",cls.money)
    @staticmethod
    def stat():
        a=10
        print("Parent static Method",a)
#child class
class child(Parent):
    def childfun(cself):
        print("child class Instance Method",cself.money)
s = child()
s.show()
s.showMoney()
s.stat()
s.childfun()
"""
"""
#Constractor with Super Method
class Parent:
    def __init__(self,m):
        self.money = m
        print("Parent class constractor")

    def show(self):
         print("Parent Class Instance Method")
   
#child class
class child(Parent):
    def __init__(self,m,n):
        super().__init__(m)
        self.job = n
        print("child class constractor")

    def childFun(cself):
        print("child class Instance Method",cself.money,"job:-",cself.job)
s = child(2000,'python')
s.childFun()
"""




"""
#multiple inheritance

#class A
class A:
    def __init__(self):
         #calling super class(object) constractor
        super().__init__()
        print("A class constractor")
        
    def showA(self):
        print("A class method")

#B class
class B:
    def __init__(self):
        #calling super class constractor
        super().__init__()
        print("B class constractor")
        
    def showB(self):
        print("B class method")

#class C
class C(A,B):
    def __init__(self):
         #calling super class constractor
        super().__init__()
        print("C class constractor")
       
    def showC(self):
        print("C class method")

objC= C()

"""
#operator overloading
print(10+20)
print(int.__add__(10,20))#magic method

print("hello"+"tanu")
print(str.__add__("hello","tanu"))

#object added