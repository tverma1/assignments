"""
import functionpro 

functionpro.moduleFun("tanu")

#variables in module
a = functionpro.student["name"]
print(a)
a = functionpro.student["age"]
print(a)
"""
"""
#Re-naming a Module
import functionpro  as fp
fp.moduleFun("tanu")


#variables in module
a = fp.student["name"]
print(a)
a = fp.student["age"]
print(a)

#platform
import platform

x = platform.system()
print(x)
"""

"""
#from used
from functionpro import student
print(student["name"])
"""

#built-in function

import random

"""
randomNumber = random.randint(0,5)
print(randomNumber)
"""

rand = random.random() # by default  0 and 1
print(rand)

rand = random.random()*100 #0 and 100
print(rand)

#give list for random choice
list = ["apple","banana","grape","apricot"]
choice = random.choice(list)
print(choice)
