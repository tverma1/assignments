list = ["apple","orange","banana","kiwi","grapes","cherry"]
print(list)
print(len(list))

#different data type
list1 =["apple","orange","banana","kiwi",1,2,3.5,True,"cherry"]
print(list1)
print(len(list1))
print(type(list))

#list() Constructor
list2 =(("apple","orange","banana","kiwi","grapes","cherry"))
print(list2)

#Access Items from start
print("index 1 item:- " +list1[1])

#Access Items from end
print("end index item:- " +list1[-1])


#Range of Indexes
#print("range from 1 to 3:- " +list[1:3])
print(list[1:3])

#skip start range
#print("skip start range and end range is 4:- " +list[:4])
print(list[:4])

#skip end range
#print("skip end range and start range is 3:- "+list[3:])
print(list[3:])

#neg range
print(list[-3:-1])

#change item value
list[1]="watermelon"
print(list)

#change a range of item values
list[2:5]=["apple","orange"]
print(list)

list[2:6]=["apple","orange"]
print(list)

#methods
#insert
list.insert(2, "kiwi")
print(list)

#append
list.append("kiwi")
print(list)

#extend
list3=["abc",1,9.6,True]
list.extend(list3)
print(list)


#remove
list.remove("kiwi")
print("remove kiwi")
print(list) 

#pop
list.pop(1)
print("pop")
print(list)

list.pop()
print("pop last")
print(list)

#del
del list[0]
print("del 0")
print(list)


#add iterable object (tuple item)
tuple = ("kiwi", "orange")
list.extend(tuple)
print(list) 


#clean
list.clear()
print("clear list")
print(list)

#del list
del list
print("delete list")
print(list)