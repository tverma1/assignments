x = 10   # int
y = 5.9 # float
z = 5j   # complex

print(type(x))
print(type(y))
print(type(z))


# float
x = 35e3
y = 12E4
z = -87.7e100

print(type(x))
print(type(y))
print(type(z))

#complex
p = 3+5j
q = 5j
r = -5j

print(type(p))
print(type(q))
print(type(r))

#type conversion
x = 10   # int
y = 5.9  # float
z = 5j   # complex

#int to float
a = float(x)

#float to int:
b = int(y)

#int to complex:
c = complex(x)

print(a)
print(b)
print(c)

print(type(a))
print(type(b))
print(type(c))