#function calling
def myFunction(name):
    print(name)
myFunction("taruna")

#Arguments *args
def myFunction(*name):
    print("*args")
    print("first-name:-\t" + name[0] + "\nlast-name:-\t" + name[1])
myFunction("taruna","verma")


#keyword argument(order of parameters not matter in this case)
def myFunction(lname,fname):
    print("kwargs")
    print("first-name:-\t" + fname + "\nlast-name:-\t" + lname)
myFunction(fname="taruna",lname="verma")

#keyword **argument(order of parameters not matter in this case)
def myFunction(**name):
    print("**kwargs")
    print("first-name:-\t" + name["fname"] + "\nlast-name:-\t" + name["lname"])
myFunction(fname="taruna",lname="verma")

#passing a list as argument
def myFunction(list):
    print("list passing")
    for x in list:
     print(x)

name=["taruna","aakansha"]
myFunction(name)

#return value
def myFunction(p):
    return 2*p
a=myFunction(4)
print(a)

#recurtion
def fibonacci(n):
    if n <=1:
        return n
    else:
        return(fibonacci(n-1)+fibonacci(n-2))
n=10
if n<=0:
    print("invalid input")
else:
    print("fibonacci series")
    for i in range(n):
        print(fibonacci(i))


#lambda function
minus = lambda x,y:x-y
print("minus",minus(2,9))        

 #module
def moduleFun(name):
    print("name:- "+name) 

#variables in module
student = {
  "name": "aanu",
  "age": 23,
  "country": "india"
}

